Source: spiped
Section: utils
Priority: optional
Maintainer: Peter Pentchev <roam@debian.org>
Build-Depends: debhelper (>= 12.8~), libssl-dev, procps
Standards-Version: 4.5.0
Homepage: https://www.tarsnap.com/spiped.html
Vcs-Git: https://gitlab.com/spiped/spiped.git -b debian/master
Vcs-Browser: https://gitlab.com/spiped/spiped/tree/debian/master
Rules-Requires-Root: no

Package: spiped
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: foreign
Description: create secure pipes between socket addresses
 spiped (pronounced "ess-pipe-dee") is a utility for creating symmetrically
 encrypted and authenticated pipes between socket addresses, so that one may
 connect to one address (e.g., a UNIX socket on localhost) and transparently
 have a connection established to another address (e.g., a UNIX socket on a
 different system).  This is similar to 'ssh -L' functionality, but does not
 use SSH and requires a pre-shared symmetric key.
 .
 spipe (pronounced "ess-pipe") is a utility which acts as an spiped protocol
 client (i.e., connects to an spiped daemon), taking input from the standard
 input and writing data read back to the standard output.
